mod rank_list;
mod tier_list;

use std::fmt;

use crate::ranks::rank_list::RANKS;
use crate::ranks::tier_list::Tier;

pub fn to_roman_numeral(num: u8) -> String {
    match num {
        1 => String::from("I"),
        2 => String::from("II"),
        3 => String::from("III"),
        4 => String::from("IV"),
        5 => String::from("V"),
        6 => String::from("VI"),
        7 => String::from("VII"),
        _ => String::from(num.to_string()),
    }
}

#[derive(Clone, Copy)]
pub struct Rank {
    pub tier: Tier,
    pub division: u8,
    pub rp_needed: u16,
    pub entry_cost: u16,
}

impl Rank {
    pub fn new(tier: Tier, division: u8, rp_needed: u16, entry_cost: u16) -> Rank {
        Rank {
            tier,
            division,
            rp_needed,
            entry_cost,
        }
    }

    pub fn from(rank_points: i32) -> Rank {
        let mut i: usize = 0;

        while i + 1 < RANKS.len() {
            if RANKS[i + 1].rp_needed > rank_points as u16 {
                return RANKS[i].clone();
            }

            i += 1;
        }

        return RANKS[i].clone();
    }
    pub fn next(rank_points: i32) -> Rank {
        let mut i: usize = 0;

        while i + 1 < RANKS.len() {
            if RANKS[i].rp_needed > rank_points as u16 {
                return RANKS[i].clone();
            }

            i += 1;
        }

        return RANKS[i].clone();
    }
}

impl fmt::Display for Rank {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(
            format!(
                "{} {}",
                self.tier.to_string(),
                to_roman_numeral(self.division)
            )
            .as_str(),
        )
    }
}
