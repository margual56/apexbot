use std::fmt;

#[derive(Clone, Copy, Debug)]
pub enum Tier {
    ROOKIE,
    BRONZE,
    SILVER,
    GOLD,
    PLATINUM,
    DIAMOND,
    MASTER,
    APEXPREDATOR,
}

impl fmt::Display for Tier {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let s = format!("{:?}", self);
        let mut c = s.chars();

        let tmp = match c.next() {
            None => String::new(),
            Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
        };

        fmt.write_str(&tmp)
    }
}
