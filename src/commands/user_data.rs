use poise::serenity_prelude::json::JsonError;
use poise::serenity_prelude::{self as serenity, UserId};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    env,
    fs::{self, File},
    time::SystemTime,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Data {
    users: HashMap<UserId, UserData>,
}

impl Data {
    pub fn new() -> Data {
        match Data::read() {
            Ok(d) => d,
            Err(e) => {
                println!("Couldn't parse data, creating a new instance: {}", e);

                let data = Data {
                    users: HashMap::new(),
                };

                data.write();

                return data;
            }
        }
    }

    pub fn read() -> Result<Data, JsonError> {
        let path = "./userdata.json";
        let data = fs::read_to_string(path).expect("Unable to read file");

        serde_json::from_str(&data)
    }

    pub fn write_user(user: UserData) {
        let mut data = Data::new();

        data.users.insert(user.discord_user.id, user);

        data.write();
    }

    pub async fn check_updates() -> Vec<(UserData, UserData)> {
        let mut data = Data::new();

        let mut updated: Vec<(UserData, UserData)> = Vec::new();

        for user in data.users.clone().into_values() {
            let elapsed = user.timestamp.elapsed().expect("Could not measure time");
            if elapsed.as_secs_f64() > 0.0
            /*10800.0*/
            {
                // 3*60*60
                if let Some(update) = user.get_update().await {
                    updated.push(update.clone());
                    data.users.insert(update.1.discord_user.id, update.1);
                }
            }
        }

        data.write();

        return updated;
    }

    pub fn write(&self) {
        serde_json::to_writer(
            File::create("./userdata.json").expect("Could not create file"),
            self,
        )
        .expect("Could not serialize Data");
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserData {
    pub discord_user: serenity::User,
    pub apex_username: String,
    pub last_rank: i32,
    pub last_level: i32,
    pub channel: serenity::ChannelId,
    pub timestamp: SystemTime,
}

impl UserData {
    pub async fn new(
        discord: serenity::User,
        apex: String,
        channel: serenity::ChannelId,
    ) -> (UserData, bool) {
        let data = Data::new();

        if let Some(user) = data.users.get(&discord.id) {
            (user.clone(), true)
        } else {
            let apex_api_key: String =
                env::var("APEX_KEY").expect("Environment variable APEX_KEY was not found");

            match apex_legends_api::get_user_retry(String::from(&apex), &apex_api_key, true).await {
                Ok(u) => (
                    UserData {
                        discord_user: discord,
                        apex_username: apex,
                        last_rank: u.global.rank.rank_score,
                        last_level: u.global.level,
                        channel,
                        timestamp: SystemTime::now(),
                    },
                    false,
                ),
                Err(_e) => (
                    UserData {
                        discord_user: discord,
                        apex_username: apex,
                        last_rank: -1,
                        last_level: -1,
                        channel,
                        timestamp: SystemTime::now(),
                    },
                    false,
                ),
            }
        }
    }

    pub async fn get_update(mut self) -> Option<(UserData, UserData)> {
        let apex_api_key: String =
            env::var("APEX_KEY").expect("Environment variable APEX_KEY was not found");

        let mut new_rank = self.last_rank;
        let mut new_level = self.last_level;

        match apex_legends_api::get_user_retry(String::from(&self.apex_username), &apex_api_key, true).await {
            Ok(apex) => {
                new_rank = apex.global.rank.rank_score;
                new_level = apex.global.level;
            }
            Err(_e) => (),
        };

        if self.last_rank != new_rank || self.last_level != new_level {
            let copy = self.clone();
            self.last_level = new_level;
            self.last_rank = new_rank;

            Some((copy, self.clone()))
        } else {
            None
        }
    }
}
