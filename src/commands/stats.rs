use crate::ranks::Rank;

use super::{Context, Error};
use std::env;

/// Información sobre el usuario en Apex
///
/// Coge información de la API de Apex
/// (https://apexlegendsstatus.com/) y hace cositas
#[poise::command(prefix_command, slash_command)]
pub async fn stats(
    ctx: Context<'_>,
    #[description = "Apex username"] username: String,
) -> Result<(), Error> {
    let apex_api_key: String =
        env::var("APEX_KEY").expect("Environment variable APEX_KEY was not found");

    match apex_legends_api::get_user(String::from(&username), &apex_api_key).await {
        Ok(data) => {
            ctx.send(|m| {
                m.embed(|e| {
                    e.thumbnail(data.global.rank.rank_img.as_str())
                        .title(format!("Stats for {}", username))
                        .field(
                            "Rank",
                            Rank::from(data.global.rank.rank_score).to_string(), /*format!(
                                                                                     "{} {} - {}",
                                                                                     data.global.rank.rank_name,
                                                                                     to_roman_numeral(data.global.rank.rank_division),
                                                                                     data.global.rank.rank_score
                                                                                 )*/
                            true,
                        )
                        .field(
                            "Level",
                            format!(
                                "{} (next: {}%)",
                                data.global.level, data.global.to_next_level_percent
                            ),
                            true,
                        )
                        .author(|f| f.icon_url(data.global.avatar))
                })
            })
            .await?;
        }
        Err(e) => {
            ctx.say(format!("There was an error: {}", e)).await?;
        }
    };

    Ok(())
}

#[poise::command(prefix_command, slash_command)]
pub async fn rank_stats(
    ctx: Context<'_>,
    #[description = "Apex username"] username: String,
) -> Result<(), Error> {
    let apex_api_key: String =
        env::var("APEX_KEY").expect("Environment variable APEX_KEY was not found");

    match apex_legends_api::get_user(String::from(&username), &apex_api_key).await {
        Ok(data) => {
            let rank = Rank::from(data.global.rank.rank_score);
            let next_rank = Rank::next(data.global.rank.rank_score);
            let perc = (data.global.rank.rank_score as f32 - rank.rp_needed as f32)
                / (next_rank.rp_needed as f32 - rank.rp_needed as f32);

            ctx.send(|m| {
                m.embed(|e| {
                    e.thumbnail(data.global.rank.rank_img.as_str())
                        .title(format!("Rank info: {}", rank))
                        .field("Name", format!("{}", rank), false)
                        .field(
                            format!("Current RP ({})", username),
                            format!("{} ({:.2}%)", data.global.rank.rank_score, perc * 100f32),
                            false,
                        )
                        .field("Current level's RP", rank.rp_needed, true)
                        .field("Next level's RP", next_rank.rp_needed, true)
                        .field("Cost per game (RP)", rank.entry_cost, false)
                        .author(|f| f.icon_url(data.global.avatar))
                })
            })
            .await?;
        }
        Err(e) => {
            ctx.say(format!("There was an error: {}", e)).await?;
        }
    };

    Ok(())
}
