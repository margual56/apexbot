use super::{user_data::Data, Context, Error};

/// Información sobre el usuario en Apex
///
/// Coge información de la API de Apex
/// (https://apexlegendsstatus.com/) y hace cositas
#[poise::command(prefix_command, slash_command)]
pub async fn update(ctx: Context<'_>) -> Result<(), Error> {
    let thing = Data::check_updates().await;

    if thing.is_empty() {
        ctx.say("Sorry, no hay ninguna actualización. La verdad es que sois bastante malos")
            .await?;
    } else {
        for (prev, new) in thing {
            ctx.say(format!(
                "Enhorabuena {}!:\nRank: {} 👉 {} \nLevel: {} 👉 {}",
                new.discord_user.name,
                prev.last_rank,
                new.last_rank,
                prev.last_level,
                new.last_level
            ))
            .await?;
        }
    }

    Ok(())
}
