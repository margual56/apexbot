mod register;
mod register_user;
mod stats;
mod update;
mod user_data;

pub use register::register;
pub use register_user::register_user;
pub use stats::{rank_stats, stats};
pub use update::update;
pub use user_data::UserData;

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Context<'a> = poise::Context<'a, Data, Error>;

// User data, which is stored and accessible in all command invocations
pub struct Data {}
