use super::{user_data::Data, Context, Error, UserData};
use poise::serenity_prelude as serenity;

/// Información sobre el usuario en Apex
///
/// Coge información de la API de Apex
/// (https://apexlegendsstatus.com/) y hace cositas
#[poise::command(prefix_command, slash_command)]
pub async fn register_user(
    ctx: Context<'_>,
    #[description = "Discord username"] disc_username: serenity::User,
    #[description = "Apex username"] apex_username: String,
) -> Result<(), Error> {
    let tag = disc_username.tag();
    let apex = String::from(&apex_username);

    let data = UserData::new(disc_username, apex_username, ctx.channel_id()).await;

    if data.1 {
        ctx.say(format!(
            "User {} was already registered as {}",
            data.0.discord_user.tag(),
            data.0.apex_username
        ))
        .await?;
        return Ok(());
    } else {
        Data::write_user(data.0);

        ctx.say(format!("Registered: {} is {} in Apex", tag, apex))
            .await?;

        return Ok(());
    }
}
