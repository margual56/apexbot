mod commands;
mod ranks;

use commands::*;
use poise::serenity_prelude as serenity;
use std::env;

#[tokio::main]
async fn main() {
    dotenv::dotenv().expect("Could not load .env file");

    println!("Running bot...");

    // Register commands and other options
    let options = poise::FrameworkOptions {
        commands: vec![stats(), rank_stats(), register_user(), register(), update()],
        prefix_options: poise::PrefixFrameworkOptions {
            prefix: Some(String::from("/")),

            mention_as_prefix: false,
            // An edit tracker needs to be supplied here to make edit tracking in commands work
            edit_tracker: Some(poise::EditTracker::for_timespan(
                std::time::Duration::from_secs(3600 * 3),
            )),
            ..Default::default()
        },

        ..Default::default()
    };

    // Create the actual framework, with the previous options
    let framework = poise::Framework::builder()
        .options(options)
        .token(env::var("DISCORD_TOKEN").expect("missing DISCORD_TOKEN"))
        .intents(
            serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT,
        )
        .user_data_setup(move |_ctx, _ready, _framework| Box::pin(async move { Ok(Data {}) }));

    println!("Now!");

    framework.run().await.expect("Client error");
}
